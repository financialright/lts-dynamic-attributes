<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use LTS\CmsModels\Models\CMS\DS2Model;

class AttributeOption extends DS2Model
{

    protected $table = 'da_attributeoption';
}
