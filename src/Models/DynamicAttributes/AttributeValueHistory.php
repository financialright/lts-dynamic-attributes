<?php


namespace LTS\CmsModels\Models\DynamicAttributes;


use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AttributeValueHistory extends DS2Model
{

    protected $table = 'da_attributevalue_history';


    public function attributeValue(): BelongsTo
    {
        return $this->belongsTo(AttributeValue::class, 'da_attributevalue_id');
    }
}