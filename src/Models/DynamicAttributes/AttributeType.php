<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use LTS\CmsModels\Models\CMS\DS2Model;

class AttributeType extends DS2Model
{

    protected $table = 'da_attributetype';


    protected $casts = [
        'attributes' => 'array',
    ];

    protected $fillable = [
        'element_type',
        'cast_type',
        'attributes',
    ];
}


