<?php


namespace LTS\CmsModels\Models\DynamicAttributes;


use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AttributeDisplayConfig extends DS2Model
{

    protected $table = 'attribute_display_config';

    public function attributeGroup(): BelongsTo
    {
        return $this->belongsTo(AttributeGroup::class, 'attributegroup_id');
    }

    public function hasOwnForm(): bool
    {
        return (bool)$this->getAttribute('has_own_form');
    }

    public function hook(): BelongsTo
    {
        return $this->belongsTo(Hook::class);
    }

}