<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use LTS\CmsModels\Models\CMS\DS2Model;

class Attribute extends DS2Model
{

    protected $table = 'da_attribute';

    public function attributeType(): BelongsTo
    {
        return $this->belongsTo(AttributeType::class, 'da_attributetype_id');
    }

    public function optionList(): BelongsTo
    {
        return $this->belongsTo(AttributeOptionlist::class, 'optionlist_id');
    }

    public function attributeValue(): HasOne
    {
        return $this->hasOne(AttributeValue::class, 'da_attribute_id');
    }


    public function isOptionList(): bool
    {
        return null !== $this->getAttribute('optionlist_id');
    }

    public function getValue(int $attributableId = null): AttributeValue
    {
        $thisId = $this->getKey();
        /**
         * @var AttributeValue|null $attributeValue
         */
        $attributeValue = $this->attributeValue()
            ->where('attributable_id', $attributableId)
            ->where('da_attribute_id', $thisId)
            ->first();

        if ($attributeValue) {
            $attributeValue->setAttribute('value', $attributeValue->getValue());
        } else {
            $attributeValue = AttributeValue::on($this->getConnectionName())->newModelInstance();
            $attributeValue->setAttribute('attributable_id', $attributableId);
            $attributeValue->setAttribute('da_attribute_id', $thisId);
        }

        return $attributeValue;
    }

    public function setValue(AttributeValue $attributeValue): bool
    {
        $attributeValue->setAttribute('da_attribute_id', $this->getKey());
        $value = $attributeValue->getAttribute('value');
        $attributeValue->setValue($value);
        return $attributeValue->save();
    }

    public function isActuallyUsedInAttributeGroupContext(): bool
    {
        return null !== $this->getAttribute('pivot');
    }

    public function getOptionList(): AttributeOptionlist
    {
        return $this->getAttribute('optionList');
    }

    public function setOptionList(AttributeOptionlist $optionList): void
    {
        $this->setAttribute('optionList', $optionList);
        $this->setAttribute('optionlist_id', $optionList->getKey());
    }

    public function getCustomElementType(): ?string
    {
        return $this->getAttributeFromAttributeGroupPivotByKey('custom_element_type');
    }

    public function getCustomTemplate(): ?string
    {
        return $this->getAttributeFromAttributeGroupPivotByKey('custom_template');
    }

    public function getSort(): ?int
    {
        return $this->getAttributeFromAttributeGroupPivotByKey('sort');
    }

    private function getAttributeFromAttributeGroupPivotByKey(string $attributeName)
    {
        $attributeValue = null;
        if ($this->isActuallyUsedInAttributeGroupContext()) {
            $pivot = $this->getAttribute('pivot');
            $attributeValue = $pivot->getAttribute($attributeName);
        }

        return $attributeValue;
    }

    public function setAttributeType(AttributeType $attributeType): void
    {
        $this->setAttribute('da_attributetype_id', $attributeType->getKey());
    }
}
