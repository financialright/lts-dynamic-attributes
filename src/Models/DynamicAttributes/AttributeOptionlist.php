<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

class AttributeOptionlist extends DS2Model
{

    protected $table = 'da_attributeoptionlist';

    public function options(): HasMany
    {
        return $this->hasMany(AttributeOption::class, 'optionlist_id');
    }

    public function isReferencedAttributeOptionList(): bool
    {
        return null !== $this->getReferencedAttributeOption();
    }

    public function getReferencedAttributeOption(): ?ReferencedAttributeOption
    {
        return $this->getAttribute('referencedAttributeOption');
    }

    public function setReferencedAttributeOption(ReferencedAttributeOption $referencedAttributeOption): void
    {
        $this->setAttribute('referenced_attribute_options_id', $referencedAttributeOption->getKey());
    }

    public function referencedAttributeOption(): HasOne
    {
        return $this->hasOne(ReferencedAttributeOption::class, 'id', 'referenced_attribute_options_id');
    }

    public function hasEmptyOption(): bool
    {
        return (bool)$this->getAttribute('has_empty_option');
    }

    public function getOptions(): Collection
    {
        $options = $this->getAttribute('options');
        if (null === $options) {
            $query = AttributeOption::query();
            $query->where('optionlist_id', $this->getKey());
            $options = $query->get();
        }

        return $options;
    }

    public function attributes(): HasMany
    {
        return $this->hasMany(Attribute::class, 'optionlist_id');
    }
}
