<?php


namespace LTS\CmsModels\Models\DynamicAttributes;


use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ReferencedAttributeOption extends DS2Model
{

    protected $table = 'da_referenced_attribute_options';

    public function attributeOptionlist(): BelongsTo
    {
        return $this->belongsTo(AttributeOptionlist::class, 'id', 'referenced_attribute_options_id');
    }

    public function setReferencedModel(string $referencedModel): void
    {
        if (!class_exists($referencedModel)) {
            throw new \InvalidArgumentException('given class not exists!');
        }

        if (!is_a($referencedModel, Model::class, true)) {
            throw new \InvalidArgumentException('given class "' . $referencedModel . '" is not a Model!');
        }

        $this->setAttribute('referenced_model', $referencedModel);
    }

    public function getReferencedModel(): ?Model
    {
        $model = null;
        $referencedModel = $this->getAttribute('referenced_model');
        if (null !== $referencedModel && class_exists($referencedModel)) {
            $model = app()->make($referencedModel);
            if ($model instanceof Model === false) {
                $model = null;
            }
        }

        return $model;
    }

    public function hasReferencedModel(): bool
    {
        return null !== $this->getReferencedModel();
    }

    public function getReferencedOptions(): array
    {
        $options = [];
        $referencedModel = $this->getReferencedModel();
        if ($referencedModel) {
            $query = $referencedModel->newQuery();
            $displayColumn = $this->getAttribute('display_column');

            $sortByColumn = $displayColumn;
            if (!empty($this->getAttribute('sort_column'))) {
                $sortByColumn = $this->getAttribute('sort_column');
            }

            if (1 === $this->getAttribute('sort_descending')) {
                $direction = 'DESC';
            } else {
                $direction = 'ASC';
            }

            $query->orderBy($sortByColumn, $direction);

            $options = $query->pluck(
                $displayColumn,
                $referencedModel->getKeyName()
            )->all();
        }

        return $options;
    }
}