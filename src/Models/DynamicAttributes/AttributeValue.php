<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AttributeValue extends DS2Model
{
    protected $table = 'da_attributevalue';

    private $valueCastTypes = [
        'json'
    ];

    protected function getValueCastTypes(): array
    {
        return $this->valueCastTypes;
    }

    protected function setValueCastTypes(array $castTypes): void
    {
        $this->valueCastTypes = $castTypes;
    }

    protected function hasValueCastType(string $type): bool
    {
        return in_array($type, $this->getValueCastTypes(), true);
    }

    protected function castValue($value, string $castType, string $fromOrTo)
    {
        $method = ($fromOrTo === 'to' ? 'as' : 'from') . ucfirst($castType);
        if (true === method_exists($this, $method)) {
            $value = $this->$method($value);
        }

        return $value;
    }

    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class, 'da_attribute_id');
    }

    public function getAttributeType(): AttributeType
    {
        return $this->getAttribute('attribute')->getAttribute('AttributeType');
    }

    public function attributeValueHistory(): HasMany
    {
        return $this->hasMany(AttributeValueHistory::class, 'da_attributevalue_id');
    }

    public function setRelatedModel(DynamicAttributeModel $model): void
    {
        /**
         * @var Attribute $attribute
         */
        $attribute = $this->getAttribute('attribute');
        $modelClass = get_class($model);
        $attributableType = $attribute->getAttribute('attributable_type');

        if ($modelClass === $attributableType) {
            $this->setAttribute('attributable_id', $model->getKey());
        } else {
            throw new \InvalidArgumentException($modelClass . ' is not a class of type ' . $attributableType);
        }
    }

    public function getRelatedModel(): DynamicAttributeModel
    {
        $id = $this->getAttribute('attributable_id');
        /**
         * @var Attribute $attribute
         */
        $attribute = $this->getAttribute('attribute');
        $attributableType = $attribute->getAttribute('attributable_type');
        /**
         * @var DynamicAttributeModel $model
         */
        $model = app()->make($attributableType);
        return $model->find($id);
    }

    public function getValue()
    {
        $attributeType = $this->getAttributeType();
        $value = $this->getAttribute('value');
        if (null !== $value) {
            $castType = $attributeType->getAttribute('cast_type');
            if ($this->hasValueCastType($castType)) {
                $value = $this->castValue($value, $castType, 'from');
            } else {
                settype($value, $castType);
            }
        }

        return $value;
    }

    public function setValue($value = null): void
    {
        $attributeType = $this->getAttributeType();
        if (null !== $value) {
            $castType = $attributeType->getAttribute('cast_type');
            if ($this->hasValueCastType($castType)) {
                $value = $this->castValue($value, $castType, 'to');
            } else {
                settype($value, $castType);
            }
        }

        $this->setAttribute('value', $value);
    }
}
