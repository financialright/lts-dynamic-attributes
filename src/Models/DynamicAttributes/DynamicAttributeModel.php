<?php


namespace LTS\CmsModels\Models\DynamicAttributes;


use LTS\CmsModels\Models\CMS\ClientProduct;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use LTS\CmsModels\Models\CMS\DS2Model;

abstract class DynamicAttributeModel extends DS2Model
{

    protected $clientProduct;
    protected $dynamicAttributes;
    protected $attributableType = '';

    public function setAttributableType(string $attributableType): void
    {
        $this->attributableType = $attributableType;
    }

    public function getAttributableType(): string
    {
        if ('' === $this->attributableType) {
            $this->setAttributableType(get_class($this));
        }

        return $this->attributableType;
    }

    /**
     * Sets ClientProduct on actual Model
     * @internal this method is called before all relational attributes are set. Work on foreign keys!
     */
    abstract protected function initClientProduct(): void;

    public function setClientProduct(ClientProduct $clientProduct = null): void
    {
        $this->clientProduct = $clientProduct;
    }

    public function getClientProduct(): ClientProduct
    {
        return $this->clientProduct;
    }

    public function hasClientProduct(): bool
    {
        return null !== $this->clientProduct;
    }

    public function setDynamicAttributes(Collection $dynamicAttributes = null): void
    {
        $this->dynamicAttributes = $dynamicAttributes;
    }

    public function hasDynamicAttributes(): bool
    {
        return null !== $this->dynamicAttributes;
    }

    /**
     * @param bool $refresh
     * @return Collection|Attribute[]|null
     */
    public function getDynamicAttributes(bool $refresh = false): ?Collection
    {
        if ((true === $refresh || false === $this->hasDynamicAttributes())) {
            try {
                $this->initClientProduct();
                $clientId = null;
                $productId = null;
                $attributeQuery = Attribute::query();

                if (true === $this->hasClientProduct()) {
                    $clientProduct = $this->getClientProduct();
                    $clientId = $clientProduct->getAttribute('fk_client_id');
                    $productId = $clientProduct->getAttribute('fk_product_id');
                }

                $attributeQuery->where('attributable_type', $this->getAttributableType());
                $attributeQuery->where('product_id', $productId);
                $attributeQuery->where(
                    static function (Builder $builder) use ($clientId) {
                        $builder->where('client_id', $clientId);
                        $builder->orWhereNull('client_id');
                    }
                );

                $this->setDynamicAttributes(
                    $attributeQuery->count() > 0 ? $attributeQuery->get() : null
                );
            } catch (\Exception $e) {
                app('log')->error($e->getMessage());
            }
        }


        return $this->dynamicAttributes;
    }

    public function getFillable(): array
    {
        $fillable = parent::getFillable();
        if (0 !== count($fillable)) {
            $dynamicAttributes = $this->getDynamicAttributes(false === $this->exists);
            if ($dynamicAttributes) {
                foreach ($dynamicAttributes as $attribute) {
                    $fillable[] = $attribute->getAttribute('name');
                }
            }
        }

        return $fillable;
    }


    public function setRawAttributes(array $attributes, $sync = false): self
    {
        parent::setRawAttributes($attributes, $sync);
        /**
         * when model was already loaded, clear dynamicAttributes and force a reload
         */
        $dynamicAttributes = $this->getDynamicAttributes(true);
        if ($dynamicAttributes) {
            /**
             * @var Collection|Attribute[] $dynamicAttributes
             */
            foreach ($dynamicAttributes as $attribute) {
                $id = $this->getKey();
                $attributeValue = $attribute->getValue($id);
                if ($attributeValue) {
                    $attributeName = $attribute->getAttribute('name');
                    $this->setAttribute(
                        $attributeName,
                        /**
                         * try to set given attribute from given fill array or get the default
                         * this method is called on loading and setting attributes.
                         */
                        ($this->getAttributes()[$attributeName] ?? $attributeValue->getAttribute('value'))
                    );
                }
            }
        }

        return $this;
    }

    public function delete(): ?bool
    {
        $result = null;
        if (true === $this->exists) {
            $dynamicAttributes = $this->getDynamicAttributes();
            if ($dynamicAttributes) {
                foreach ($dynamicAttributes as $dynamicAttribute) {
                    $attributeValue = $dynamicAttribute->getValue($this->getKey());
                    if ($attributeValue->exists) {
                        $tempResult = $attributeValue->delete();
                        if (true === $tempResult) {
                            $result = $tempResult;
                        }
                    }
                }
            }
        }

        if (true === $result) {
            $result = parent::delete();
        }


        return $result;
    }

    public function save(array $options = []): bool
    {
        /**
         * get all dynamic fields at first, and clean up attributes array,
         * otherwise there will be a fatal exception, because eloquent tries to handle all attributes
         * as columns on insert or update.
         */
        $dynamicFields = [];
        $dynamicAttributes = $this->getDynamicAttributes(false === $this->exists);
        if ($dynamicAttributes) {
            foreach ($dynamicAttributes as $dynamicAttribute) {
                $dynamicFieldName = $dynamicAttribute->getAttribute('name');
                $dynamicFields[$dynamicFieldName] = [
                    'attribute' => $dynamicAttribute,
                    'value' => null
                ];

                if (array_key_exists($dynamicFieldName, $this->attributes)) {
                    $dynamicFields[$dynamicFieldName]['value'] = $this->attributes[$dynamicFieldName];
                    unset($this->attributes[$dynamicFieldName]);
                }
            }
        }

        $result = parent::save($options);
        if (true === $result) {
            $this->fireModelEvent('dynamicAttributesSaving', false);
            /**
             * when we know the id of this object,
             * we will and can save all dynamic attribute values
             */
            foreach ($dynamicFields as $dynamicFieldName => $dynamicAttributeData) {
                /**
                 * @var Attribute $dynamicAttribute
                 */
                $dynamicAttribute = $dynamicAttributeData['attribute'];
                $attributeValue = $dynamicAttribute->getValue($this->getKey());
                $attributeValue->setAttribute('value', $dynamicAttributeData['value']);
                $tmpResult = $dynamicAttribute->setValue($attributeValue);
                if (true === $result) {
                    $result = $tmpResult;
                }
            }

            if (true === $result) {
                $this->fireModelEvent('dynamicAttributesSaved', false);
            }
        }

        if (true === $result) {
            $this->refresh();
        }


        return $result;
    }
}