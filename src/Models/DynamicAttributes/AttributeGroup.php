<?php

namespace LTS\CmsModels\Models\DynamicAttributes;

use LTS\CmsModels\Models\CMS\ClientProduct;
use Illuminate\Database\Eloquent\Builder;
use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class AttributeGroup.
 *
 * @package App
 */
class AttributeGroup extends DS2Model
{

    /**
     * Database connection.
     *
     * @var string
     */
    protected $table = 'da_attributegroup';

    /**
     * Get related attributes.
     *
     */
    public function attributes(): BelongsToMany
    {
        return $this->belongsToMany(
            Attribute::class,
            'da_attribute_attributegroup',
            'da_attributegroup_id',
            'da_attribute_id'
        )->withPivot(['sort', 'custom_element_type', 'custom_template']);
    }

    public function addDynamicAttribute(
        Attribute $attribute,
        int $sort = 0,
        string $customElementType = null,
        string $customTemplate = null
    ): void {
        $this->attributes()->attach(
            $attribute->getKey(),
            [
                'sort' => $sort,
                'custom_element_type' => $customElementType,
                'custom_template' => $customTemplate,
            ]
        );
    }

    public function removeDynamicAttribute(Attribute $attribute): bool
    {
        return $this->attributes()->detach([$attribute->getKey()]) > 0;
    }

    public function removeDynamicAttributeById(int $attributeId): bool
    {
        $result = false;
        /**
         * @var Attribute $attribute
         */
        $attribute = Attribute::on($this->getConnectionName())->find($attributeId);
        if ($attribute instanceof Attribute) {
            $result = $this->removeDynamicAttribute($attribute);
        }
        return $result;
    }

    /**
     * @param ClientProduct $clientProduct
     * @return Attribute[]
     */
    public function getDynamicAttributes(ClientProduct $clientProduct = null): array
    {
        $query = $this->attributes();

        if (null !== $clientProduct) {
            $productId = $clientProduct->getAttribute('fk_product_id');
            $clientId = $clientProduct->getAttribute('fk_client_id');
        } else {
            $productId = $this->getAttribute('product_id');
            $clientId = $this->getAttribute('client_id');
        }

        $query->where('product_id', $productId);
        $query->where(
            static function (Builder $builder) use ($clientId) {
                $builder->where('client_id', $clientId);
                $builder->orWhereNull('client_id');
            }
        );

        return $query->orderBy('sort')->get()->all();
    }
}
