<?php


namespace LTS\CmsModels\Models\DynamicAttributes;


use LTS\CmsModels\Models\CMS\DS2Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Hook extends DS2Model
{

    protected $table = 'hook';

    protected $fillable = [
        'name',
    ];

    public function attributeDisplayConfigs(): HasMany
    {
        return $this->hasMany(AttributeDisplayConfig::class);
    }
}